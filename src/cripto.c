#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <err.h>

int main(void);

int
main(void)
{

	/*
	 * Os tipos size_t e ssize_t são tipos portáveis e estão
	 * declarados no HEADER stdint.h
	 */
	size_t len1;
	ssize_t len2, c;
	char *str = NULL;

	len1 = 0;
	printf("Digite um nome:\n");
	/*
	 * A função getline, lê uma string de um arquivo (stream) e está
	 * definida no HEADER stdio.h.
	 * Ela necessita dsos seguintes argumentos:
	 * str	-> ponteiro para char, será alocado internamente na função
	 * len1	-> retornará o tamanho da string + '\0'
	 * stdin	-> ponteiro para um arquivo aberto (stream)
	 */
	if ( (len2 = getline(&str, &len1, stdin)) < 0 )
			  err(-1,"Input error");
	/*
	 * Loop para cifrar a mensagem
	 */
	c = 0;
	while ( c < len2 )
	{
			  /*
				* Operação XOR, faz a codificação
				*/
			  str[c] = str[c] ^ 0x55;
			  c ++;
	}
	printf("cifrado: %s\n",str);

	/*
	 * Loop para descifrar a mensagem
	 */
	c = 0;
	while ( c < len2 )
	{
			  /*
				* Operação XOR, faz a codificação
				*/
			  str[c] = str[c] ^ 0x55;
			  c ++;
	}
	printf("descifrado: %s\n",str);

	/*
	 * Libera o buffer alocado na função getline
	 */
   free(str);
   return 0;
}
