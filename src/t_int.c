#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(void);

int
main(void)
{
 
    printf("CHAR_BIT       %d\n"		, CHAR_BIT       );
    printf("MB_LEN_MAX     %d\n"		, MB_LEN_MAX     );
    printf("CHAR_MIN       %d\n"		, CHAR_MIN       );
    printf("CHAR_MAX       %d\n"		, CHAR_MAX       );
    printf("SCHAR_MIN      %d\n"		, SCHAR_MIN      );
    printf("SHRT_MIN       %d\n"		, SHRT_MIN       );
    printf("INT_MIN        %d\n"		, INT_MIN        );
    printf("LONG_MIN       %ld\n"	, LONG_MIN       );
    printf("LLONG_MIN      %lld\n"	, LLONG_MIN      );
    printf("SCHAR_MAX      %d\n"		, SCHAR_MAX      );
    printf("SHRT_MAX       %d\n"		, SHRT_MAX       );
    printf("INT_MAX        %d\n"		, INT_MAX        );
    printf("LONG_MAX       %ld\n"	, LONG_MAX       );
    printf("LLONG_MAX      %lld\n"	, LLONG_MAX      );
    printf("UCHAR_MAX      %u\n"		, UCHAR_MAX      );
    printf("USHRT_MAX      %u\n"		, USHRT_MAX      );
    printf("UINT_MAX       %u\n"		, UINT_MAX       );
    printf("ULONG_MAX      %lu\n"	, ULONG_MAX      );
    printf("ULLONG_MAX     %llu\n"	, ULLONG_MAX     );
    printf("PTRDIFF_MIN    %ld\n"	, PTRDIFF_MIN    );
    printf("PTRDIFF_MAX    %ld\n"	, PTRDIFF_MAX    );
    printf("SIZE_MAX       %lu\n"	, SIZE_MAX       );
    printf("SIG_ATOMIC_MIN %ld\n"	, SIG_ATOMIC_MIN );
    printf("SIG_ATOMIC_MAX %ld\n"	, SIG_ATOMIC_MAX );
    printf("WCHAR_MIN      %d\n"		, WCHAR_MIN      );
    printf("WCHAR_MAX      %d\n"		, WCHAR_MAX      );
    printf("WINT_MIN       %d\n"		, WINT_MIN       );
    printf("WINT_MAX       %d\n"		, WINT_MAX       );
	 return 0;
}

