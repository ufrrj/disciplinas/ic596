#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

/* 
 * A função err(), executa uma chamada para uma função onde
 * mostrar o nome do programa, a mensagem de error e retorna
 * para o sistema operacional o código que foi passado.
 * Esta função, assim como printf e scanf, utiliza parâmetros
 * variáveis ( ... ), ou seja, ela precia receber ao menos, dois
 * parametros, eval e fmt, os outros podem ou não existirem.
 * Essa função necessita do HEADER err.h
 *
 * O prototype da função é o seguinte:
 * void err(int eval, const char *fmt, ... );
 *
 * A função malloc() e calloc() alocam, de forma dinâmica um espaço
 * em memória para ser utilizado, e devem ser desalocado com o uso 
 * da função free().
 * A diferença principal entre malloc e calloc, é que a primeira
 * apenas aloca o espaço desejado, já calloc além de alocar o espaço
 * o faz inicializando todo o espaço com 0
 * Ambas as funções necessitam do HEADER stdlib.h
 *
 * prototype das funções
 * void *malloc(size_t size);
 * void *calloc(size_t elem, size_t size);
 *
 * A função memcpy() copia o conteúde de uma região de memória para 
 * outra, limitado ao tamanho desejado.  O destino DEVE comportar o
 * tamanho a ser copiado, na sua integralidade.
 * Esta função necessita do HEADER string.h
 *
 * prototype da função
 * void *memcpy(void *dst, void *src, size_t len);
 *
 */
int main(void);

int
main(void)
{
		  char *p;
		  char *str="Ola mundo";
		  char *str1="Prova Segunda chamada";
		  int len;

		  /*
			* Alocação dinâmica de espaço em memória
			* com 100 elementos do tamanho do tipo char
			*/
		  p = (char *)(calloc(100,sizeof(char)));
		  /*
			* Verifica se foi alocado corretamente
			*/
		  if ( p == (char *)(NULL) )
					 err(-1, "Falha na alocação");
		  printf("Enderecos de:\np    = %p\nstr  = %p\nstr1 = %p\n",
								p, str, str1);
		  len = strlen(str);
		  memcpy(p,str,len);
		  len = strlen(str1);
		  memcpy(p,str1,len);
		  printf("%s\n",p);
		  free(p);
		  return 0;
}

