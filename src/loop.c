#include <stdio.h>
#include <stdlib.h>

int main(void);

int
main(void)
{
	int a,b,c;

	printf("Programa para testar diversos tipos de loop\n");
	printf("Digite 2 valores inteiros\n");
	scanf("%d %d",&a, &b);
 	if ( a > b )
	{
		c = a;
		a = b;
		b = c;
	}
	c = a;
	printf("A = %d B = %d C = %d\n", a, b, c);
	while ( a < b )
	{
		printf( "While %d \n", a);
		a = a + 1; // a++;
	}
	a = c;
	do
	{
		printf( "do ... while %d \n", a);
		a = a + 1; // a++
	}while(a < b );

	a =c;
	for ( c = a; c < b; c++)
	{
		printf("for %d \n", c);
	}
	return 0;
}

