#include <stdio.h>
#include <stdlib.h>

/*
 * As operações aritiméticas tem prioridades, são elas:
 * +--------------------------------+-----------+-----------+
 * |	Operadores							| Tipo		| Avaliação |
 * +--------------------------------+-----------+-----------+
 * |	()  []  ->  . 						| binário	| e-d			|
 * |	- ++ -- ! & * ~ (type) sizeof	| unário		| d-e			|
 * |	*  /  % 								| binário	| e-d			|
 * |	+  -									| binário	| e-d			|
 * |	<<  >>								| binário	| e-d			|
 * |	<  <=  >=  >						| binário	| e-d			|
 * |	==  !=								| binário	| e-d			|
 * |	& | ^									| binário	| e-d			|
 * |	^										| binário	| e-d			|
 * |	|										| binário	| e-d			|
 * |	&&										| binário	| e-d			|
 * |	||										| binário	| e-d			|
 * |	? :									| ternário	| d-e			|
 * |	=  op =								| binário	| d-e			|
 * |	,										| binário	| e-d			|
 * +--------------------------------+-----------+-----------+
 * Onde 
 * e-d -> Da esquerda para a direita
 * d-e -> Da direita para a esquerda
 * Fonte: 
 * 	https://www.ime.usp.br/~pf/algoritmos/apend/precedence.html
 */		
/*
 * decimal	Hexadecimal	Binário
 * 	0			0x00		0000 0000
 * 	1			0x01		0000 0001
 * 	2			0x02		0000 0010
 * 	3			0x03		0000 0011
 * 	4			0x04		0000 0100
 * 	5			0x05		0000 0101
 * 	6			0x06		0000 0110
 * 	7			0x07		0000 0111
 * 	8			0x08		0000 1000
 * 	9			0x09		0000 1001
 * 	10			0x0A		0000 1010
 * 	11			0x0B		0000 1011
 * 	12			0x0C		0000 1100
 * 	13			0x0D		0000 1101
 * 	14			0x0E		0000 1110
 * 	15			0x0F		0000 1111
 *
 * 				0xF7		1111 0111
 */
/*
 * Lembrete!
 * Qualquer valor difernte de 0 (zero) é considerado verdadeiro, 
 * o valor 0 (zero) é considerado falso nas operações lógicas
 */
int main(void);

int
main(void)
{
	int a, b, c;
	a = 0xF5;
	b = 0xA5;
	c = 0x70;
	printf("Operadores Bitwise\n");
	printf("         0x%X  | 0x%X  = 0x%X\n", a, b, a  | b );
	printf("         0x%X  & 0x%X  = 0x%X\n", a, b, a  & b );
	printf("  0x%X | 0x%X  & 0x%X  = 0x%X\n", a, b, c, a | b & c);
	printf("  0x%X | (0x%X & 0x%X) = 0x%X\n", a, b, c, a | (b & c));
	printf(" (0x%X | 0x%X) & 0x%X  = 0x%X\n", a, b, c, (a | b) & c);
	printf("                ~0x%X  = 0x%X\n", a, ~a );
	printf("        ~0x%X  & 0x%X  = 0x%X\n", a, b, ~a & b);
	printf("       ~(0x%X  & 0x%X) = 0x%X\n", a, b, ~(a & b));
	printf("       (~0x%X) & 0x%X  = 0x%X\n", a, b, (~a) & b);


	printf("\nOperadores Logicos\n");
	printf("   0x%X || 0x%X  = 0x%X\n", a, b, a || b );
	printf("   0x%X && 0x%X  = 0x%X\n", a, b, a && b );
	printf("          !0x%X  = 0x%X\n", a, !a );
	printf("  !0x%X  & 0x%X  = 0x%X\n", a, b, !a & b );
	printf(" !(0x%X  & 0x%X) = 0x%X\n", a, b, !(a & b) );
	printf(" (!0x%X) & 0x%X  = 0x%X\n", a, b, (!a) & b );
	return 0;
}

