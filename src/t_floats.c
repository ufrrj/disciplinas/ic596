#include <float.h>
#include <stdio.h>
#include <stdlib.h>

int main(void);

int
main(void)
{
 
	 printf("FLT_RADIX        %16d\n"		, FLT_RADIX        );
    printf("DECIMAL_DIG      %16d\n"		, DECIMAL_DIG      );
    printf("FLT_DECIMAL_DIG  %16d\n"		, FLT_DECIMAL_DIG  );
    printf("DBL_DECIMAL_DIG  %16d\n"		, DBL_DECIMAL_DIG  );
    printf("LDBL_DECIMAL_DIG %16d\n"		, LDBL_DECIMAL_DIG );
    printf("FLT_MIN          %16.12f\n"	, FLT_MIN          );
    printf("DBL_MIN          %16.12lf\n"	, DBL_MIN          );
    printf("LDBL_MIN         %16.12Lf\n"	, LDBL_MIN         );
    printf("FLT_TRUE_MIN     %16.12f\n"	, FLT_TRUE_MIN     );
    printf("DBL_TRUE_MIN     %16.12lf\n"	, DBL_TRUE_MIN     );
    printf("LDBL_TRUE_MIN    %16.12Lf\n"	, LDBL_TRUE_MIN    );
    printf("FLT_MAX          %16.12f\n"	, FLT_MAX          );
    printf("DBL_MAX          %16.12lf\n"	, DBL_MAX          );
    printf("LDBL_MAX         %16.12Lf\n"	, LDBL_MAX         );
    printf("FLT_EPSILON      %16.12f\n"	, FLT_EPSILON      );
    printf("DBL_EPSILON      %16.12lf\n"	, DBL_EPSILON      );
    printf("LDBL_EPSILON     %16.12Lf\n"	, LDBL_EPSILON     );
    printf("FLT_DIG          %16d\n"		, FLT_DIG          );
    printf("DBL_DIG          %16d\n"		, DBL_DIG          );
    printf("LDBL_DIG         %16d\n"		, LDBL_DIG         );
    printf("FLT_MANT_DIG     %16d\n"		, FLT_MANT_DIG     );
    printf("DBL_MANT_DIG     %16d\n"		, DBL_MANT_DIG     );
    printf("LDBL_MANT_DIG    %16d\n"		, LDBL_MANT_DIG    );
    printf("FLT_MIN_EXP      %16d\n"		, FLT_MIN_EXP      );
    printf("DBL_MIN_EXP      %16d\n"		, DBL_MIN_EXP      );
    printf("LDBL_MIN_EXP     %16d\n"		, LDBL_MIN_EXP     );
    printf("FLT_MIN_10_EXP   %16d\n"		, FLT_MIN_10_EXP   );
    printf("DBL_MIN_10_EXP   %16d\n"		, DBL_MIN_10_EXP   );
    printf("LDBL_MIN_10_EXP  %16d\n"		, LDBL_MIN_10_EXP  );
    printf("FLT_MAX_EXP      %16d\n"		, FLT_MAX_EXP      );
    printf("DBL_MAX_EXP      %16d\n"		, DBL_MAX_EXP      );
    printf("LDBL_MAX_EXP     %16d\n"		, LDBL_MAX_EXP     );
    printf("FLT_MAX_10_EXP   %16d\n"		, FLT_MAX_10_EXP   );
    printf("DBL_MAX_10_EXP   %16d\n"		, DBL_MAX_10_EXP   );
    printf("LDBL_MAX_10_EXP  %16d\n"		, LDBL_MAX_10_EXP  );
    printf("FLT_ROUNDS       %16d\n"		, FLT_ROUNDS       );
    printf("FLT_EVAL_METHOD  %16d\n"		, FLT_EVAL_METHOD  );
    printf("FLT_HAS_SUBNORM  %16d\n"		, FLT_HAS_SUBNORM  );
    printf("DBL_HAS_SUBNORM  %16d\n"		, DBL_HAS_SUBNORM  );
    printf("LDBL_HAS_SUBNORM %16d\n"		, LDBL_HAS_SUBNORM );
	 return 0;
}

