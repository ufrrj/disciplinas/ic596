#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void);

int
main(void)
{
	char valor[65]; // 1 caracter por bit + 1 para o '\0'
	int counter, tamanho, a;
	unsigned int decimal;

	/*
	 * Loop para limpar a string ( vetor ), colocando o caracter
	 * NULL ( '\0' ) em todas as posições
	 */
	for ( counter = 0; counter < 65; counter ++ )
		valor[counter] = '\0';
	
	printf("Digite um valor binario com no maximo 64bits\n");
	scanf("%s",valor);
	/*
	 * Detecta o comprimento em "bits" da string
	 */
	counter = 0;
	while ( valor[counter] != '\0' )
		counter ++;

	/*
	 * Transforma o binário em decimal
	 */
	tamanho = counter;
	decimal = 0;
	for ( ; counter ; counter -- )
	{
		/*
		 * Exemplo: valor digitado - 101
		 * tamanho = 3; counter = 3 => 3 - 3 = 0
		 * 2^0 = 1 => 1 * a = a
		 * próximo passo: counter = 2
		 * tamanho - counter = 3 - 2 = 1
		 * 2^1 = 2 => 2 * a = 2a
		 * próximo passo: counter = 1
		 * tamanho - counter = 3 - 1 = 2
		 * 2^2 = 4 => 4 * a = 4a
		 * decimal = 1a + 2a + 4a = 5
		 */
		a = valor[tamanho - counter] - '0';
		decimal = decimal + a * pow(2, counter-1);
	}
	printf("Valor digitado em binario = %s resultado em decimal = %u \n", 
						 valor, decimal );

	return 0;
}

