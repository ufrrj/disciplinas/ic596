#include <stdio.h>
#include <stdlib.h>

int main(void);

int
main(void)
{
		  FILE		*fp;
		  char 		*fn;
		  size_t		slen;
		  ssize_t	len;

		  slen = 0;
		  fn=NULL;

		  printf("Digite o nome do arquivo: ");
		  if ( (len = getline(&fn, &slen, stdin)) == -1 )
		  {
					 printf("Erro na leitura do nome do arquivo\n");
					 exit( -1 );
		  }

		  // substituindo o '\n' por '\0' 
		  fn[len-1] = '\0';

		  if ((fp = fopen(fn, "rt")) == (FILE *)(NULL))
		  {
					 printf("Arquivo [ %s ] nao existe!\n",fn);
					 exit(-2);
		  }
		  while ( !feof(fp) )
		  {
					 len = getline(&fn, &slen, fp);
					 if ( len < 0 )
								break;
					 fwrite(fn, len , 1, stdout );
					 // Semelhante ao printf
					 // printf("%s", fn);
		  }

		  fclose(fp);
		  free(fn);
		  return 0;
}
