#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/*
 * Utilizando um vetor de ponteiros para função
 * veja mais detalhes no arquivo pointer_funcao.c
 */
int main(void);

// Prototypes das funções
uint8_t	funcao1(uint8_t a);
uint8_t	funcao2(uint8_t a);
uint8_t	funcao3(uint8_t a);
uint8_t	funcao4(uint8_t a);

// Listas das funções
uint8_t
funcao1(uint8_t a)
{
		  printf("Funcao 1 - Valor de A = %u\n", a);
		  return a;
}

uint8_t
funcao2(uint8_t a)
{
		  printf("Funcao 2 - Valor de A = %u\n", a);
		  return a;
}

uint8_t
funcao3(uint8_t a)
{
		  printf("Funcao 3 - Valor de A = %u\n", a);
		  return a;
}

uint8_t
funcao4(uint8_t a)
{
		  printf("Funcao 4 - Valor de A = %u\n", a);
		  return a;
}


int
main(void)
{
		  uint8_t (*p[4])(uint8_t);
		  uint8_t c;

		  // Atribuição dos valores de cada função para o respectuvi
		  // ponteiro
		  p[0] = funcao1;
		  p[1] = funcao2;
		  p[2] = funcao3;
		  p[3] = funcao4;

		  // Simulando a chamada de cada uma das funções atraves dos
		  // indeces do vetor
		  for ( c=0; c< 4; c++)
			  printf("Valor de retorno da funcao %u\n",(*p[c])(c));
		  return 0;
}
