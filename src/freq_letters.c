#include <stdio.h>		// prototypes das funcoes de entrada e saida
#include <stdlib.h>		// prototypes das funcoes da biblioteca padrao

/*
 * prototype das funcoes utilizadas
 *
 * int printf(const char *format, ... );
 * char *gets_s(char *str, size_t size);
 */

#define MAX_STR_LEN 1024	// Definindo constantes

int main(void);			// prototype da funcao main

int							// funcao main
main(void)
{
		  /*
			* Esse programa mostra duas maneiras de contar letras em
			* um texto digitado pelo usuárioo. A variável freq é um
			* vetor multidimensional de duas dimensões, onde a primeira
			* dimensão é a forma que foi contado e a segunda dimensão
			* é a frequência das letras.
			*/
		 char texto[MAX_STR_LEN + 1];	// string com o tamanho definido
												// pela constante + 1
		 /*
		  * Arrays com o alfabeto de duas formas para que seja possível
		  * mostrar duas maneiras diferentes de contar as letras de
		  * um texto.
		  */
		 char alf1[52]="aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ";
		 char alf2[52]="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

		 int freq[2][26];
		 int counter1, counter2;
													

		 printf("Programa para contar a frequencia de letras em uma frase\n");

		 printf("Digite um texto\n");
		 gets_s(texto, MAX_STR_LEN);

		 /*
		  * inicializa o vetor de duas dimensões com o valor 0
		  */
		 for (counter1=0; counter1 < 26; counter1 ++)
		 {
				 freq[0][counter1] = 0;
				 freq[1][counter1] = 0;
		 }

		 printf("Texto[ %d ] = %s\n", counter1, texto);
		 printf("Alfabetos\n%s\n%s\n", alf1, alf2);
		 counter1 = 0;
		 /*
		  * primeiro método de cálculo
		  */
		 while ( texto[counter1] )
		 {
					for ( counter2 = 0; counter2 < 52; counter2 += 2)
							  if ( (texto[counter1] == alf1[counter2 + 0]) ||
									 (texto[counter1] == alf1[counter2 + 1]))
										 freq[0][(int)(counter2/2)] ++;
					counter1 ++;
		 }
		 /*
		  * segundo método de cálculo
		  */
		 counter1 = 0;
		 while ( texto[counter1] )
		 {
					for ( counter2 = 0; counter2 < 26; counter2 ++)
							  if ((texto[counter1] == alf2[counter2 +  0]) ||
									(texto[counter1] == alf2[counter2 + 26]))
										 freq[1][counter2] ++;
					counter1 ++;
		 }
       counter1 = 0;		 
		 /*
		  * Mostra a frequência de cada letra encontrada no texto.
		  * Não distingue entre maiúsculas e minúsculas.
		  */
		 while ( counter1 < 26 )
		 {
					printf(" %c = %d - %d \n", 
										 'a'+counter1, freq[0][counter1], 
										 freq[1][counter1]);
					counter1 ++;
		 }
		 return 0;
}
