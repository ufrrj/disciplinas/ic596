#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Este arquivo demonstra a utilização da função fprintf, que
 * tem como primeiro argumento, um descritor de arquivos aberto
 * onde será escrita a mensagem. o seu prototype é:
 *
 * int fprintf( FILE * restrict stream, const char * restric *fmt, ...)
 * 
 * stream 	- ponteiro do arquivo aberto.  Caso o arquivo não esteja aberto
 * 				um erro será reportado e a função retornará -1
 * fmt		- string contendo a formatação, identica ao printf
 *
 * Mais informações: man 3 fprintf
 *
 */
int main(int argc, char *argv[]);

int
main(int argc, char *argv[])
{
		  FILE *fp;			// Ponteiro para o arquivo
		  char fn[2048];	// buffer para armazenar o nome do arquivo a ser
								// aberto

		  printf("Demonstracao do uso da funcao: fprintf\n");

		  if ( argc > 1 )	// Caso haja parâmetros na linha de comando
								// Copia o primeiro para o buffer ( fn )
					 memcpy(fn, argv[1], strlen(argv[1]));
		  else				// Caso contrário, use um arquivo padrão  (built-in)
					 memcpy(fn, "/tmp/teste.txt", 15);
		  fp = fopen(fn, "wt");	// Abre o arquivo para escrita

		  // Escreve em stdout ( saída padrão )
		  fprintf(stdout,"Saida direcionada para o descritor de arquivo stdout");
		  // Escreve em stderr ( saída de erro padrão )
		  fprintf(stdout,"Saida direcionada para o descritor de arquivo stderr");

		  // Escreve no arquivo aberto por fopen
		  fprintf(fp, "Saida redirecionada para o arquivo %s\n", fn);
		  fclose(fp);

		  return 0;
}
