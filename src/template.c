/*
 * Cabeçalhos de bibliotecas, os chamados HEADERS
 *
 * std -> standart
 * 	|
 * 	+-	io -> InputOutput ( Entrada e saida )
 * 	+-	lib -> Biblioteca padrão
 * 	+- int -> tipos gerais, como size_t, uint8, 
 * 				dentre outros
 */ 
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/*
 * Prototype da função main
 * a função main é a principal função de um programa
 * na linguagem C/C++
 */
int main(void);

/*
 * Corpo da função main
 */
int
main(void)
{
		  /*
			* A identação é necessária para uma melhor leitura
			* dos comandos dados
			*/
		  return 0;
}

