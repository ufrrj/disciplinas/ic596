#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/*
 * Programa para demonstração da função sprintf
 *
 * int sprintf(char restrict *str, char restrict *fmt, ... )
 * A função sprintf escreve em um buffer (str) o que foi passado
 * pela string de formatação (fmt) que seque os padrões das funções
 * printf. Veja mais informação em: man 3 sprintf
 * A função retorna quantos caracteres foram escritos no buffer, sem
 * contar o caracter '\0'
 */


int main(void);

int
main(void)
{
		  char buffer[100];
		  uint16_t	c;
		  int		len;

		  printf("Uso da funcao sprintf\n");

		  for ( c = 0; c < 10; c ++)
		  {
					 len = sprintf(buffer,"./teste-%05u.tmp", c);
					 printf("Nome do arquivo: %s - %d\n", buffer, len);
		  }

		  return 0;
}
