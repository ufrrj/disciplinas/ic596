#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

/*
 * Para evitar que sistemas que ainda não tenham a função
 * gets_s implementada, possa funcionar perfeitamente.
 * A função gets não é mais recomendada pelo fato de gerar
 * buffer overflow. Ela é uma função que não oferece
 * segurança para digitação de textos longos
 */

#ifndef __FreeBSD__
#define gets_s((X), (Y))	gets((X))
#endif

/*
 * Definição de uma constante para criação de um buffer de tamanho
 * suficiente para a digitação de um texto longo
 */
#define MAX_STR_LEN	1024

int main(void);

int
main(void)
{
	char a[MAX_STR_LEN];
	unsigned int c;

	/*
	 * Inicialização do buffer
	 */
	for ( c = 0; c < MAX_STR_LEN; c++)
			  a[c] = '\0';

	printf("Digite um texto com 4 caracteres:\n");
	/*
	 * Teste necessário para saber se houve um dos seguintes casos de 
	 * erro:
	 * 	n == 0			-> Se foi passado 0 como quantidade de caracteres
	 * 	buffer == NULL -> Se o ponteiro do buffer não foi alocado
	 * 	Se foi digitado mais caracteres do que n
	 */
	if ( gets_s(a, 5) == NULL ) 
			  err(-1, "Error on gets_s: %s", a);
	printf("Voce digitou: %s len = %lu\n",a, strlen(a));
	return 0;
}

