#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void);

int
main(void)
{
		  char ch[100]; // Vetor de caracteres ou string
		  int vec[100]; // Vetor de inteiros
		  unsigned char imagem1[1024][768]; // Imagem com 8bits de cor
														// Maxtriz duas dimensoes 
														// primeira 1024 (x)
														// Segunda 768   (y)
		  unsigned char imagem2[1024][768][3]; // Imagem RGB
															// Matriz três deimensões
															// primeira 1024 (x)
															// segunda 768   (y)
															// terceira 3    (canais de cor)
		  /*
			* Criação de estruturas:
			* struct - São tipos de dados especiais, onde organizamos variáveis
			* 	de tipos diferentes
			* union - Semelhante a struct, porém compartilha o espaço de memória
			*  com as variáveis que a compõe
			*
			*
			* Criação do tipo Struct Identificacao
			*/
		  struct Identificacao
		  {
					 int id;
					 char nome[104];
					 unsigned char idade;
		  };

		  /*
			* Criação da variável id do tipo Struct identificacao
			*/
		  struct Identificacao id;		// Variável normal 
		  struct Identificacao *id2;	// Variável tipo ponteiro

		  struct linteiro {
					 long int a;
		  };
		  struct inteiro {
					 int a;
					 int b;
		  } ;
		  struct byte {
					unsigned char b0;
					unsigned char b1;
					unsigned char b2;
					unsigned char b3;
					unsigned char b4;
					unsigned char b5;
					unsigned char b6;
					unsigned char b7;
		  } ;
		  /*
			* Union é uma estrutura (struct) onde seus membros compartilham
			* a mesma memória. O tamanho a union é a do tipo maior.
			*/
		  union ident 
		  {
					 struct linteiro la;
					 struct inteiro a;
					 struct byte b;
		  };

		  union ident u;

		  /* Uso da estrutura (struct) de variável normal, usa o ponto (.)
			* para acessar os elementos dentro da estrutura
			*/
		  id.id = 0;
		  id.idade = 80;
		  strcpy(id.nome,"Fulano");

		  printf("Registro ID: Id = %d\n Nome = %s,Idade = %u\n", id.id,
								id.nome, id.idade);

		  /*
			* Inicializando o ponteiro da estrutura id2 com o endereço da 
			* estrutura id
			*/
		  id2 = &id;
		  printf("Registro ID2 = &ID: Id = %d\n Nome = %s,Idade = %u\n", id2->id,
								id2->nome, id2->idade);

		  /*
			* Alocando espaço dinamicamente da strutura
			*/
		  id2 = calloc(1, sizeof(struct Identificacao));
		  printf("Registro ID2 = &ID: Id = %d\n Nome = %s,Idade = %u\n", id2->id,
								id2->nome, id2->idade);
		  /*
			* Para acessar os membros da estrutura do tipo ponteiro, usae-se
			* os sinais de menos seguido do sinal de maior (->) que indica que
			* o elemento encontra-se apontado pelo ponteiro da estrutura
			*/
		  id2->id= 1;
		  id2->idade = 50;
		  strcpy(id2->nome,"Beltrano");

		  printf("Registro ID2 inicializado: Id = %d\n Nome = %s,Idade = %u\n", id2->id,
								id2->nome, id2->idade);


		  printf("%p %lu %lu %lu %lu\n", id2, sizeof(struct Identificacao),
								sizeof(id.id), sizeof(id.nome), sizeof(id.idade));
		  /*
			* Como foi alocado espaço dinamicamente, é necessário libera-lo
			* antes de terminar a execução do programa.
			*/
		  free(id2);

		  /* 
			* Nova alocação memória, desta vez com malloc
			*/
		  id2 = malloc(sizeof(struct Identificacao) * 1);
		  printf("%p %lu %lu %lu %lu\n", id2, sizeof(struct Identificacao),
								sizeof(id.id), sizeof(id.nome), sizeof(id.idade));
		  printf("Registro ID2 = &ID: Id = %d\n Nome = %s,Idade = %u\n", id2->id,
								id2->nome, id2->idade);

		  free(id2);

		  printf("%lX\n", sizeof(union ident));
		  u.a.a = 0x0A;
		  printf("%02X %02X %02X %02X\n", u.b.b0, u.b.b1, u.b.b2, u.b.b3);
		  u.a.a = 0x0A00;
		  printf("%02X %02X %02X %02X\n", u.b.b0, u.b.b1, u.b.b2, u.b.b3);
		  u.a.a = 0x0A0000;
		  printf("%02X %02X %02X %02X\n", u.b.b0, u.b.b1, u.b.b2, u.b.b3);
		  u.a.a = 0x0A000000;
		  printf("%02X %02X %02X %02X\n", u.b.b0, u.b.b1, u.b.b2, u.b.b3);

		  u.la.a = 0x0A00A00550607080;
		  printf("%X %X\n", u.a.a, u.a.b);

		  return 0;
}
