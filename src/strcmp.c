#include <stdio.h>
#include <stdlib.h>
#include <string.h>	// Necessario para as funcoes de strings

/*
 * Exemplo de uso das funções strcmp e strncmp, que tem o seguinte 
 * comportamento:
 *
 * 	strcmp - compara duas strings ( str1 e str2 ), e retorna valor menor
 * 			que zero ( <0 ), igual a zero ( =0 ) ou maior que zero ( >0 )
 * 			no caso de str1 for menor, igual ou maior que str2.
 * 	strncmp - compara duas strings ( str1 e str2 ), limitando a essa 
 * 			comparação aos N primeiros caracteres, e retorna valor menor
 * 			que zero ( <0 ), igual a zero ( =0 ) ou maior que zero ( >0 )
 * 			no caso de str1 for menor, igual ou maior que str2.
 *
 * typedef da função:
 *
 * 	int	strcmp ( const char *dst, const char *src)
 * 	int	strncmp( const char *dst, const char *src, size_t maxlen)
 *
 */

int main(void);

int
main(void)
{
		  char *str1, *str2;
		  int	s;
		  size_t len;

		  // Alocação de 1024 bytes para a string, a área alocada 
		  // retoran com conteúdo lixo
		  str1 = (char *)(malloc(sizeof(char) * 1024));
		  str2 = (char *)(malloc(sizeof(char) * 1024));

		  printf("Digite uma string (20 caracteres): ");
		  gets_s(str1, 20);

		  printf("Digite outra string (20 caracteres): ");
		  gets_s(str2, 20);

		  printf("str1 = %s\nstr2 = %s\n", str1, str2);
		  if ( ( s = strcmp( str1, str2 ) )> 0 )
					 printf("str1 > str2\n");
		  if ( s < 0 )
					 printf("str1 < str2\n");
		  if ( s == 0 )
					 printf("str1 = str2\n");

		  printf("Digite a quantidade de caracteres a comparar: ");
		  scanf("%lu", &len);
		  if ( ( s = strncmp( str1, str2, len ) )> 0 )
					 printf("str1 > str2\n");
		  if ( s < 0 )
					 printf("str1 < str2\n");
		  if ( s == 0 )
					 printf("str1 = str2\n");

		  // Liberando a memória alocada
		  free(str1);
		  free(str2);

		  return 0;
}
