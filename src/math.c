#include <stdio.h>
#include <stdlib.h>
// Arquivos com as definições das funções matemáticas e constantes, como
// por exemplo o PI ( M_PI )
#include <math.h>

/*
 * Funções:
 *
 * Para maiores informações, use man 3 math.
 *
 * Os nomes das funções podem ter um sufixo f ou l, que significam que o parametro 
 * passado pode ser float ou long double, sem esse sufixo, o tipo é double
 *
 * 	Funções de potencia:
 * 	double		pow (double x		, double y		)		-> x^y
 * 	float			powf(float x		, float y		)		-> x^y
 * 	long double	powl(long double x, long double y)		-> x^y
 *
 * 	Exponenciação
 * 	double		exp (double x)									-> e^x
 * 	double		exp2 (double x)								-> 2^x
 *
 * 	Funções trigonométricas:
 * 	As funções trigonométricas utilizam o angulo em radianos
 * 	Além do sufixo f ou l, usam o sufixho h para operações hiperbólicas e tem 
 * 	o prefixo a, que refere-se ao inverso da função
 *
 * 	double 		cos (double a)									-> cosseno(a)
 * 	double 		sin (double a)									-> seno(a)
 * 	double 		tan (double a)									-> tangente(a)
 *
 * 	double 		acos (double a)								-> secante(a)
 * 	double 		asin (double a)								-> cossecante(a)
 * 	double 		atan (double a)								-> cotangente(a)
 *
 * 	double 		cosh (double a)								-> cosseno hiperbólico(a)
 * 	double 		sinh (double a)								-> seno hiperbólico(a)
 * 	double 		tanh (double a)								-> tangente hiperbólico(a)
 *
 * 	double 		acosh (double a)								-> secante hiperbólico(a)
 * 	double 		asinh (double a)								-> cossecante hiperbólico(a)
 * 	double 		atanh (double a)								-> cotangente hiperbólico(a)
 */
int main(void);

int
main(void)
{
		  int a, b;
		  float c, d, e;

		  printf("Digite um valor para a: ");
		  scanf("%d", &a);
		  printf("Digite um valor para b: ");
		  scanf("%d", &b);

		  printf("Digite um valor para c: ");
		  scanf("%f", &c);
		  printf("Digite um valor para d: ");
		  scanf("%f", &d);

		  printf("a ^ b = %f\t%3.1f\n", pow(a, b), pow(a, b) );
		  printf("a ^ b = %f\t%3.1f\n", powf(a, b), powf(a, b) );
		  printf("a ^ b = %15.13Lf\t%3.1Lf\n", powl(a, b), powl(a, b) );

		  printf("c ^ d = %f\t%3.1f\n", pow(c, d), pow(c, d) );
		  printf("c ^ d = %f\t%3.1f\n", powf(c, d), powf(c, d) );
		  printf("c ^ d = %15.13Lf\t%3.1Lf\n", powl(c, d), powl(c, d) );

		  printf("Digite um algulo em graus: ");
		  scanf("%f", &c);
		  printf("Digite um angulo em graus: ");
		  scanf("%f", &d);

		  /*
			* 180 - PI
			* X	- Y
			* X = 180 * Y / PI -> Radianos para Graus
			* Y = PI * X / 180 -> Graus para Radianos
			*/

		  e = cos(c);
		  printf("Cos ( %20.18f ) = %20.18f => cos-1 =  %20.18f\n", c, e, acos(e)  );
		  d = d * M_PIl /180.0;
		  e = cos(d);
		  printf("Cos ( %20.18f ) = %20.18f => cos-1 = %20.18f \n", d, e, acos(e) );

		  e = sin(c);
		  printf("Sen ( %20.18f ) = %20.18f => sin-1 =  %20.18f\n", c, e, asin(e)  );
		  e = sin(d);
		  printf("Sen ( %20.18f ) = %20.18f => sin-1 = %20.18f \n", d, e, asin(e) );

		  e = tan(c);
		  printf("Tg ( %20.18f ) = %20.18f => tan-1 =  %20.18f\n", c, e, atan(e)  );
		  e = tan(d);
		  printf("Tg ( %20.18f ) = %20.18f => tan-1 = %20.18f \n", d, e, atan(e) );

		  printf("Digite uma potencia para as funcoes exp e exp2: ");
		  scanf("%d", &a);

		  printf(" e ^ %d = %f  <=> 2 ^ %d = %f\n", a, exp(a), a, exp2(a) );

		  return 0;
}
