#include <stdio.h>
#include <stdlib.h>

/*
 * As operações aritiméticas tem prioridades, são elas:
 * +--------------------------------+-----------+-----------+
 * |	Operadores							| Tipo		| Avaliação |
 * +--------------------------------+-----------+-----------+
 * |	()  []  ->  . 						| binário	| e-d			|
 * |	- ++ -- ! & * ~ (type) sizeof	| unário		| d-e			|
 * |	*  /  % 								| binário	| e-d			|
 * |	+  -									| binário	| e-d			|
 * |	<<  >>								| binário	| e-d			|
 * |	<  <=  >=  >						| binário	| e-d			|
 * |	==  !=								| binário	| e-d			|
 * |	&										| binário	| e-d			|
 * |	^										| binário	| e-d			|
 * |	|										| binário	| e-d			|
 * |	&&										| binário	| e-d			|
 * |	||										| binário	| e-d			|
 * |	? :									| ternário	| d-e			|
 * |	=  op=								| binário	| d-e			|
 * |	,										| binário	| e-d			|
 * +--------------------------------+-----------+-----------+
 * Onde 
 * e-d -> Da esquerda para a direita
 * d-e -> Da direita para a esquerda
 * Fonte: 
 * 	https://www.ime.usp.br/~pf/algoritmos/apend/precedence.html
 */		
int main(void);

int
main(void)
{
	int a,b,c;

	a = 5;
	b = 2;
	c = 3;
	printf("Prioridade das operacoes aritmeticas\n");
	printf("Diferenças entre uso de valores inteiros e decimais\n");
	printf("Diferenca entre arredondar e \"truncar\"\n");
	printf("        %d + %d   * %d  = %d\n"	, a,b,c, a + b * c);
	printf("       (%d + %d)  * %d  = %d\n"	, a,b,c, (a + b) * c);
	printf("        %d / %d   * %d  = %d\n"	, a,b,c, a / b * c);
	printf("        %d / %d.0 * %d  = %5.2f\n", a,b,c, a / (b*1.) * c);
	printf("        %d / %d.0 * %d  = %2.0f\n", a,b,c, a / (b*1.) * c);
	printf("        %d / %d.0 * %d  = %5.2f\n", a,c,b, a / (c*1.) * b);
	printf("        %d / %d.0 * %d  = %2.0f\n", a,c,b, a / (c*1.) * b);
	printf("  (int)(%d / %d.0 * %d) = %d\n"	, a,c,b, (int)(a / (c*1.) * b));
	printf("  (int)(%d / %d.0 * %d) = %d\n"	, a,b,c, (int)(a / (b*1.) * c));
	printf("(float)(%d / %d.0 * %d) = %f\n"	, a,b,c, (float)(a / (b*1.) * c));
	printf("(float)(%d / %d.0 * %d) = %.2f\n"	, a,b,c, (float)(a / (b*1.) * c));
	return 0;
}

