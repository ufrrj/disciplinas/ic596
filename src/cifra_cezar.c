#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAX_STR_LEN	1024

#ifndef __FreeBSD__
#define gets_s((X),(Y)) get((X))
#endif

int main(void);

int
main(void)
{
	char texto[MAX_STR_LEN];
	char cifra[26]="EFGHIJKLMNOPQRSTUVWXYZABCD";
	int counter;

	printf("Cifra de Cezar\nDigite um texto\n");
	// gets_s(texto, MAX_STR_LEN);
	gets_s(texto, MAX_STR_LEN);

	counter = 0;
	while( texto[counter] )
	{
		if ( isalpha(texto[counter]) )
			texto[counter] = cifra['z' - tolower(texto[counter])];
		counter ++;
	}
	printf("Cifra = %s\n", texto);

	return 0;
}
