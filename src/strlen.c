#include <stdio.h>
#include <stdlib.h>
#include <string.h>	// Necessario para as funcoes de strings

/*
 * Exemplo de uso das funções strlen e strnlen, que retorna o comprimento de
 * uma string.
 *
 * typedef da função:
 *
 * 	size_t	strlen( const char *str)
 * 	size_t	strnlen( const char *str, size_t maxlen)
 *
 */

int main(void);

int
main(void)
{
		  char *str;
		  size_t	s;

		  // Alocação de 1024 bytes para a string, a área alocada 
		  // retoran com conteúdo lixo
		  str = (char *)(malloc(sizeof(char) * 1024));
		  s = strlen(str);	// retorna o tamanho = 1024
		  printf("%lu - %s\n", s, str);
		  s = strnlen(str, 10);	// retorna o tamanho = 10
		  printf("%lu - %s\n", s, str);
		  free(str);

		  // Alocação de 1024 bytes para a string, a área alocada 
		  // retoran com conteúdo zerado
		  str = (char *)(calloc(sizeof(char), 1024));
		  s = strlen(str);	// retorna o tamanho = 0
		  printf("%lu - %s\n", s, str);
		  s = strnlen(str, 10);	// retorna o tamanho = 0
		  printf("%lu - %s\n", s, str);
		  free(str);

		  return 0;
}
