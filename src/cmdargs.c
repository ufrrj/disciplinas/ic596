#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>

int main(int argc, char *argv[]);
void usage(char *str);

void usage(char *str)
{
		  printf("Usar %s com as seguintes opcoes: -b -f arquivo\n", str);
		  exit(0);
}

int
main(int argc, char *argv[])
{

     int bflag, ch, fd;
	  int counter;
	  printf("A linha de comando tem %d argumentos\n", argc);

     bflag = 0;
	  counter = 0;
     while ((ch = getopt(argc, argv, "hbf:")) != -1) {
				 printf("Argumento: %d = %c %s %s\n", 
									  counter++, ch, *optarg?"argumento= ":"", optarg);
             switch (ch) {
             case 'b':
                     bflag = 1;
                     break;
             case 'f':
                     if ((fd = open(optarg, O_RDONLY, 0)) < 0) {
                             (void)fprintf(stderr,
                                 "myname: %s: %s\n", optarg, strerror(errno));
                             exit(1);
                     }
                     break;
             case '?':
				 case 'h':
             default:
                     usage(argv[0]);
             }
     }
     argc -= optind;
     argv += optind;
	  
	  return 0;
}
