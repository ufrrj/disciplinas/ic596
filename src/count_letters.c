#include <stdio.h>		// prototypes das funcoes de entrada e saida
#include <stdlib.h>		// prototypes das funcoes da biblioteca padrao

/*
 * prototype das funcoes utilizadas
 *
 * int printf(const char *format, ... );
 * char *gets_s(char *str, size_t size);
 */

#define MAX_STR_LEN 1024	// Definindo constantes

int main(void);			// prototype da funcao main

int							// funcao main
main(void)
{
		  char texto[MAX_STR_LEN + 1];	// string com o tamanho definido
													// pela constante + 1
		  int counter;

/*
 * Esse codigo demonstra varias formas de se usar a condicao de parada
 * do loop, utilizando-se apenas a condicao:
 * == 0 -> falso -> para o loop
 * != 0 -> verdadeiro -> o loop continua
 */

		  printf("Digite um texto\n");
		  gets_s(texto, MAX_STR_LEN);
		  printf("Texto = %s\n", texto);
		  for (counter=0; texto[counter]; counter++);
		  printf("Comprimento, em letras, do texto = %d\n", counter);
		  for (counter=-1; texto[++counter]; );
		  printf("Comprimento, em letras, do texto = %d\n", counter);
		  counter = 0;
		  while ( texto[counter] )
					 counter ++;
		  printf("Comprimento, em letras, do texto = %d\n", counter);
		  counter = 0;
		  while ( texto[counter] != '\0' )
					 counter ++;
		  printf("Comprimento, em letras, do texto = %d\n", counter);
		  for (counter=0; texto[counter] != '\0'; counter++);
		  printf("Comprimento, em letras, do texto = %d\n", counter);

		  return 0;
}
