#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAX_STR_LEN	1024

#ifndef __FreeBSD__
#define gets_s((X),(Y))	gets((X))
#endif

int main(void);

int
main(void)
{
	char texto[MAX_STR_LEN];
	char cifra[25][26]={
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ",
		"BCDEFGHIJKLMNOPQRSTUVWXYZA",
		"CDEFGHIJKLMNOPQRSTUVWXYZAB",
		"DEFGHIJKLMNOPQRSTUVWXYZABC",
		"FGHIJKLMNOPQRSTUVWXYZABCDE",
		"GHIJKLMNOPQRSTUVWXYZABCDEF",
		"HIJKLMNOPQRSTUVWXYZABCDEFG",
		"IJKLMNOPQRSTUVWXYZABCDEFGH",
		"JKLMNOPQRSTUVWXYZABCDEFGHI",
		"KLMNOPQRSTUVWXYZABCDEFGHIJ",
		"LMNOPQRSTUVWXYZABCDEFGHIJK",
		"MNOPQRSTUVWXYZABCDEFGHIJKL",
		"NOPQRSTUVWXYZABCDEFGHIJKLM",
		"OPQRSTUVWXYZABCDEFGHIJKLMN",
		"PQRSTUVWXYZABCDEFGHIJKLMNO",
		"QRSTUVWXYZABCDEFGHIJKLMNOP",
		"RSTUVWXYZABCDEFGHIJKLMNOPQ",
		"STUVWXYZABCDEFGHIJKLMNOPQR",
		"TUVWXYZABCDEFGHIJKLMNOPQRS",
		"UVWXYZABCDEFGHIJKLMNOPQRST",
		"VWXYZABCDEFGHIJKLMNOPQRSTU",
		"WXYZABCDEFGHIJKLMNOPQRSTUV",
		"XYZABCDEFGHIJKLMNOPQRSTUVW",
		"YZABCDEFGHIJKLMNOPQRSTUVWX",
		"ZABCDEFGHIJKLMNOPQRSTUVWXY"};

	int counter, opcao;

	printf("Cifra de Cezar\nDigite um texto\n");
	gets_s(texto, MAX_STR_LEN);
	opcao = 0;
	do
	{
		printf("Digite um valor entre 0 e 24\n");
		scanf("%d", &opcao);
	}
	while( opcao < 0 || opcao > 24);

	counter = 0;
	while( texto[counter] )
	{
		if ( isalpha(texto[counter]) )
			texto[counter] = cifra[opcao][tolower(texto[counter]) - 'a'];
		counter ++;
	}
	printf("Cifra = %s\n", texto);

	return 0;
}
