#include <stdio.h>
#include <stdlib.h>
#include <string.h>	// Necessario para as funcoes de strings

/*
 * Exemplo de uso das funções strcpy e strncpy, strcmp e strncmp, 
 * que tem o seguinte comportamento:
 *
 * 	strcpy - copia todos os caractes da string origem (src)
 * 			para a string destino (dst), retornando o ponteiro para
 * 			a string destino
 * 	strncpy - copia os N primeiros caractes da string origem (src)
 * 			para a string destino (dst), retornando o ponteiro para
 * 			a string destino. Não garante a presença do '\0' no final
 * 			da string de destino
 *
 * typedef da função:
 *
 * 	char *strcpy ( char restrict *dst, const char *src)
 * 	char *strncpy( char restrict *dst, const char *src, size_t maxlen)
 *
 */

int main(void);

int
main(void)
{
		  char *str1, *str2;
		  size_t len;

		  // Alocação de 1024 bytes para a string, a área alocada 
		  // retoran com conteúdo lixo
		  str1 = (char *)(malloc(sizeof(char) * 1024));
		  str2 = (char *)(malloc(sizeof(char) * 1024));

		  printf("Digite uma string (20 caracteres): ");
		  gets_s(str1, 20);
		  strcpy(str2, str1);
		  printf("str1 = %s\nstr2 = %s\n", str1, str2);

		  printf("Digite a quantidade de caracteres a copiar: ");
		  scanf("%lu", &len);
		  strncpy(str2, str1, len);
		  printf("Sem o caracter \'\\0\'\nstr1 = %s\nstr2 = %s\n", str1, str2);
		  str2[len] = '\0';
		  printf("Com o caracter \'\\0\'\nstr1 = %s\nstr2 = %s\n", str1, str2);

		  free(str1);
		  free(str2);

		  return 0;
}
