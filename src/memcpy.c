#include <stdio.h>
#include <stdlib.h>
#include <string.h>	// Necessario para as funcoes de strings

/*
 * Exemplo de uso das funções strcpy e strncpy, strcmp e strncmp, 
 * que tem o seguinte comportamento:
 *
 * 	memcpy - copia os N primeiros caractes da string origem (src)
 * 			para a string destino (dst), retornando o ponteiro para
 * 			a string destino
 * 	mempcpy - copia os N primeiros caractes da string origem (src)
 * 			para a string destino (dst), retornando o ponteiro para
 * 			a posição seguinte. 
 * 			retorna *(src + N)
 *
 * typedef da função:
 *
 * 	void *memcpy ( void *dst, const void *src, size_t len)
 * 	void *mempcpy( void *dst, const void *src, size_t len)
 *
 */

int main(void);

int
main(void)
{
		  char *str1, *str2, *str3;
		  size_t len, fix;

		  // Alocação de 1024 bytes para a string, a área alocada 
		  // retoran com conteúdo lixo
		  str1 = (char *)(malloc(sizeof(char) * 1024));
		  str2 = (char *)(malloc(sizeof(char) * 1024));

		  len = -1;
		  fix = 30;
		  printf("Digite uma string (%lu caracteres): ", fix);
		  gets_s(str1, fix);
		  memcpy(str2, str1, fix);
		  printf("str1 = %s\nstr2 = %s\n", str1, str2);

		  do
		  {
					 printf("Digite a quantidade de caracteres a copiar: (max: %lu) ", len);
					 scanf("%lu", &len);
		  }
		  while(len >= fix );
		  memcpy(str2, str1, len);
		  printf("--- memcpy\n");
		  printf("Sem o caracter \'\\0\'\nstr1 = %s\nstr2 = %s\n", 
								str1, str2);
		  str2[len] = '\0';
		  printf("Com o caracter \'\\0\'\nstr1 = %s\nstr2 = %s\n", 
								str1, str2);
		  printf("Endereco str1 = %p str2 = %p\n", str1, str2);
		  printf("--- mempcpy\n");
		  // Restaurar str2. Tirar o '\0' colocado anteriormente
		  memcpy(str2, str1, fix);
		  str3 = mempcpy(str2, str1, len);
		  printf("Sem o caracter \'\\0\'\nstr1 = %s\nstr2 = %s\nstr3 = %s\n", 
								str1, str2, str3);
		  str2[len] = '\0';
		  printf("Com o caracter \'\\0\'\nstr1 = %s\nstr2 = %s\nstr3 = %s\n", 
								str1, str2, str3);
		  printf("Endereco str1 = %p str2 = %p str3 = %p\n", str1, str2, str3);

		  free(str1);
		  free(str2);

		  return 0;
}
