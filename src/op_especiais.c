#include <stdio.h>
#include <stdlib.h>

/*
 * As operações aritiméticas tem prioridades, são elas:
 * +--------------------------------+-----------+-----------+
 * |	Operadores							| Tipo		| Avaliação |
 * +--------------------------------+-----------+-----------+
 * |	()  []  ->  . 						| binário	| e-d			|
 * |	- ++ -- ! & * ~ (type) sizeof	| unário		| d-e			|
 * |	*  /  % 								| binário	| e-d			|
 * |	+  -									| binário	| e-d			|
 * |	<<  >>								| binário	| e-d			|
 * |	<  <=  >=  >						| binário	| e-d			|
 * |	==  !=								| binário	| e-d			|
 * |	&										| binário	| e-d			|
 * |	^										| binário	| e-d			|
 * |	|										| binário	| e-d			|
 * |	&&										| binário	| e-d			|
 * |	||										| binário	| e-d			|
 * |	? :									| ternário	| d-e			|
 * |	=  op =								| binário	| d-e			|
 * |	,										| binário	| e-d			|
 * +--------------------------------+-----------+-----------+
 * Onde 
 * e-d -> Da esquerda para a direita
 * d-e -> Da direita para a esquerda
 * Fonte: 
 * 	https://www.ime.usp.br/~pf/algoritmos/apend/precedence.html
 */		

int main(void);

int
main(void)
{
		  int a,b;

		  printf("Operadores unarios\n");
		  printf("Pre operadores ++ e -- \n");

		  a = 2;
		  b = 6;

		  printf("A = %d B = %d\n", a, b);
		  printf("++A = %d ++B = %d\n", ++a, ++b);
		  printf("A = %d => B = ++A = ", a);
		  b = ++a;
		  printf("%d\n", b ); 
		  printf("A = %d => B = A++ = ", a);
		  b = a++;
		  printf("%d\n", b ); 
		  printf("A = %d\n", a ); 
		  return 0;
}
