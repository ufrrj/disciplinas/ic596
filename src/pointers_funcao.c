#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/*
 * Ponteiro para função é uma estrutura de dados capaz de auxiliar
 * a construção de programas onde o uso de um switch () possa ser 
 * trocado por um vetor de ponteiro para função.
 *
 * Não é muito simples de compreender e tem seus usos em casos bem
 * específicos, mas dificulta e muito a leitura do código em si.
 * Outro ponto negativo importante é que a checagem dos parametros
 * não é feita pelo compilador.
 *
 * Como criar o ponteiro de funções:
 *
 * TIPO (*mome)(parametros)
 *
 * Exemplo:
 *
 * 	void (*p)(void)			-> ponteiro para uma função que não tem 
 * 						valor de retorno e nem recebe parametro algum
 * 	void (*p)(int a, int b) -> ponteiro para uma função que não tem
 * 						valor de retorno e recebe 2 parametros inteiros
 * 	int (*p)(double *a)		-> ponteiro para uma função que retorna
 * 						um valor inteiro e recebe como parametro um
 * 						ponteiro para double.
 */

// Prototypes das funções
int main(void);
uint8_t	funcao(uint8_t a);

// Função apenas didática
uint8_t
funcao(uint8_t a)
{
		  printf("Valor de A = %u\n", a);
		  return a*a;
}


int
main(void)
{
		  /*
			* Criação do ponteiro para a função é semelhante ao prototype
			* da função.  A utulização dos () envolvento o nome da variável
			* é necessário.
			*/

		  uint8_t (*p)(uint8_t);

		  p = funcao; // Atribui o endereço da função para o ponteiro

		  printf("Valor de retorno da funcao %u\n",(*p)(10));
		  return 0;
}
