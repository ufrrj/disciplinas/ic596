#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void);
void foo(void);
void bar(void);

/*
 * Variáveis globais são declaradas neste escopo
 * Em todo o código elas podem ser acessadas
 */
int a;

void
foo(void)
{
		  /*
			* A variável b é local a função foo() e só existirá enquando
			* esta função for executada e ao término de sua execução
			* a variável b será desalocada (destruida).
			*/
		  int b;

		  b = -1;
		  printf("%s => Valor A = %d B = %d\n", "foo", a, b);
		  a = 5;
}

void
bar(void)
{
		  /*
			* As variáveis a e b são locais a função bar() e só existiram 
			* enquando esta função for executada e ao término de sua 
			* execução as variáveis serão desalocadas (destruidas).
			*/
		  int a, b;
		  a = b = 70;
		  printf("%s => Valor A = %d B = %d\n", "bar", a, b);
		  a = 50;
}

int
main(void)
{
		  /*
			* Variáveis declaradas dentro função, só são visíveis
			* dentro da função
			*/
		  int b;

		  printf("Teste de Escopo de Variaveis\n");

		  a = 10;
		  b = 5;

		  printf("%s => Valor A = %d B = %d\n", "main", a, b);
		  foo();
		  printf("%s => Valor A = %d B = %d\n", "main", a, b);
		  bar();
		  printf("%s => Valor A = %d B = %d\n", "main", a, b);

		  return 0;
}
