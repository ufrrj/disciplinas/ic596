#include <stdio.h>
#include <stdlib.h>

/*
 * Demonstação de como se compila um projeto dividido em vários
 * arquivos.
 * Cada arquivo deve ter a sua contrapartida .h para que tudo funcione 
 * bem.
 *
 * No momento da compilação, deverá ser compilado cada arquivo do projeto
 * separadamente com as opções de linha de comando -o e -c, como no exemplo
 * abaixo:
 *
 * 	clang -o main.o -c main.c
 *
 * Depois de se obter todos os arquivos .o, basta agora realizar a linkagem
 * estática com o comando abaixo:
 *
 * 	clang -o main main.o A.o B.o .....
 *
 * Com esse comando obtemos um executável estático, com todas as funções 
 * em um só arquivo.
 * Existe outra maneira de fazer isso, utilizando a linkagem dinâmica, mas para
 * isso é preciso criar uma bibliteca compartilhável, lib.so
 */
#include "lib.h"

int main(void);

int
main(void)
{
		  int a, b;

		  banana = 1; 
		  printf("Banana = %d\n", banana);
		  funcao(a, b);
		  printf("Banana = %d\n", banana);
		  return 0;
}
