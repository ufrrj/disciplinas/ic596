#include <stdio.h>
#include <stdlib.h>

/*
 * Programa para verificar se um arquivo existe e caso não
 * exista, cria, armazenando valores em modo texto e modo
 * binário
 *
 * Funções utilizadas:
 *
 * FILE *fopen(const char *path, const char *mode) 
 *		A função fopen abre um arquivo informado no argumento path
 *	com o modo de abertura informado em mode. Os modos podem ser:
 *
 *		r		- leitura
 *		w		- escrita, caso o arquivo exista, este é truncado para tamanho 0,
 *			sobreescrevendo o arquivo antigo
 *		a		- inclusão, caso o arquivo exista, é posicionado o cursor para
 *			o fim do arquivo, sem trunca-lo.  Este modo mantém o conteúdo antigo
 *			e adiciona o novo.
 *		suffixos
 *			+		- se seguindo os modos w ou r, implica em um arquivo de leitura
 *				e escrita.
 *			b		- binário, atualmente ignorado, mantido para retrocompatibi-
 *				lidade
 *
 * int fprintf( FILE *fp, const char *restrict fmt, ... )
 * 	Função semelhante a printf, onde deve ser informado o poiteiro para
 * 		o arquivo a ser utilizado. 
 * 	Utilizando o stdout como arquivo de saída, a função fprintf tem o 
 * 	comportamente semelhante ao printf
 * 	fprintf( stdout, ... ) = printf( ... )
 *
 * int fclose( FILE *fp )
 * 	Esta função fecha o arquivo aberto
 *
 * Mais informações, man 3 fprintf
 */
int main(void);

int
main(void)
{
		  FILE		*fp;
		  char 		*fn;
		  size_t		slen;
		  ssize_t	len;
		  int			c;

		  slen = 0;
		  fn=NULL;

		  // Loop para verificar se o arquivo existe, para não sobrescrever-lo
		  do
		  {
					 printf("Digite o nome do arquivo: ");
					 // Le o nome do arquivo, pode-se usar a função
					 // gets_s, neste caso deve-se alocar a memória antes
					 if ( (len = getline(&fn, &slen, stdin)) == -1 )
					 {
								printf("Erro na leitura do nome do arquivo\n");
								exit( -1 );
					 }
					 
					 fn[len-1] = '\0';	// substitui o '\n' por '\0'

					 // Verifica se o arquivo existe
					 // Testa para saber se o arquivo existe, tentando ler o
					 // arquivo
					 if ((fp = fopen(fn, "r")) != (FILE *)(NULL))
					 {
								printf("Arquivo [ %s ] existe!\n",fn);
								fclose(fp);
					 }
		  }
		  while ( len <= 1 || fp != (FILE *)(NULL) );

		  // Abre o arquivo para escrita
		  fp = fopen( fn, "wt" );
		  
		  // Escreve os valores de 0 até 9 em modo texto
		  for ( c = 0; c < 10000; c+=1000 )
					 // igual ao printf, caso fp seja igual a stdout
					 // fprintf( stdout, "%d", c);
					 // printf("%d", c);
					 fprintf(fp, "%X ", c );

		  // Escreve os valores de 0 até 9 em modo binário
		  for ( c = 0; c < 10000; c+=1000 )
					 fwrite( &c, sizeof(int), 1, fp);

		  // Fecha o arquivo
		  fclose(fp);
		  // Libera a memória alocada
		  free(fn);
		  return 0;
}
