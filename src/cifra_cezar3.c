#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAX_STR_LEN	1024

#ifndef __FreeBSD__
#define gets_s((X), (Y))	gets((X))
#endif

int main(void);

int
main(void)
{
	char texto[MAX_STR_LEN];
	int counter, opcao, diff, aux;

	printf("Cifra de Cezar\nDigite um texto\n");
	gets_s(texto, MAX_STR_LEN);
	opcao = 0;
	do
	{
		printf("Digite um valor maior que 0 (zero)\n");
		scanf("%d", &opcao);
	}
	while( opcao <= 0 );

	printf("Texto = %s\n", texto);
	opcao = opcao % 26;
	counter = 0;
	while( texto[counter] )
	{
		if ( isalpha(texto[counter]) )
		{
			aux = tolower(texto[counter]);
			if ( (aux + opcao) > 'z')
			{
				diff = aux + opcao -'z';
				texto[counter] = 'A' + diff - 1;
			}
			else
				texto[counter] = toupper(aux + opcao);
		}
		counter ++;
	}
	printf("Cifra = %s\n", texto);

	return 0;
}
