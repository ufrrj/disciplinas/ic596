#include <stdio.h>
#include <stdlib.h>

/*
 * Programa para verificar se um arquivo existe e caso 
 * exista, abre para leitura, utilizando os dados gravados
 * pelo arquivo file_save.c
 *
 * Funções utilizadas:
 *
 * FILE *fopen(const char *path, const char *mode) 
 *		A função fopen abre um arquivo informado no argumento path
 *	com o modo de abertura informado em mode. Os modos podem ser:
 *
 *		r		- leitura
 *		w		- escrita, caso o arquivo exista, este é truncado para tamanho 0,
 *			sobreescrevendo o arquivo antigo
 *		a		- inclusão, caso o arquivo exista, é posicionado o cursor para
 *			o fim do arquivo, sem trunca-lo.  Este modo mantém o conteúdo antigo
 *			e adiciona o novo.
 *		suffixos
 *			+		- se seguindo os modos w ou r, implica em um arquivo de leitura
 *				e escrita.
 *			b		- binário, atualmente ignorado, mantido para retrocompatibi-
 *				lidade
 *
 * int fscanf( FILE *fp, const char *restrict fmt, ... )
 * 	Função semelhante a scanf, onde deve ser informado o ponteiro para
 * 		o arquivo (fp) a ser utilizado. 
 * 	Utilizando o stdout como arquivo de saída, a função fprintf tem o 
 * 	comportamente semelhante ao printf
 * 	fprintf( stdout, ... ) = printf( ... )
 *
 * size_t fread(void *restrict buff, size_t size, size_t nelem, FILE *fp)
 * 	Funçãoo fread le de um arquivo (fp) a quantidade de elementos (nelem)
 * 	de tamanho especificado (size), armazenando no buffer (buff)
 *
 * int fclose( FILE *fp )
 * 	Esta função fecha o arquivo aberto
 *
 * Mais informações, man 3 fprintf
 */
int main(void);

int
main(void)
{
		  FILE		*fp;
		  char 		*fn;
		  size_t		slen;
		  ssize_t	len;
		  int			a,c;

		  slen = 0;
		  fn=NULL;

		  // Loop para verificar se o arquivo existe, para não sobrescrever-lo
		  do
		  {
					 printf("Digite o nome do arquivo: ");
					 // Le o nome do arquivo, pode-se usar a função
					 // gets_s, neste caso deve-se alocar a memória antes
					 if ( (len = getline(&fn, &slen, stdin)) == -1 )
					 {
								printf("Erro na leitura do nome do arquivo\n");
								exit( -1 );
					 }
					 
					 fn[len-1] = '\0';	// modifica o '\n' por '\0'

					 // Verifica se o arquivo existe
					 // Testa para saber se o arquivo existe, tentando ler o
					 // arquivo
					 if ((fp = fopen(fn, "r")) == (FILE *)(NULL))
					 {
								printf("Arquivo [ %s ] NAO existe!\n",fn);
								fclose(fp);
					 }
		  }
		  while ( fp == (FILE *)(NULL) );

		  // Abre o arquivo para escrita
		  fp = fopen( fn, "rt" );
		  
		  // Escreve os valores de 0 até 9 em modo texto
		  for ( c = 0; c < 10; c++ )
		  {
					 fscanf(fp, "%X ", &a );
					 printf("%d\n", a);
		  }

		  printf("---\n");

		  // Escreve os valores de 0 até 9 em modo binário
		  for ( c = 0; c < 10; c++ )
		  {
					 fread( &a, sizeof(int), 1, fp);
					 printf("%d\n", a);
		  }

		  // Fecha o arquivo
		  fclose(fp);
		  // Libera a memória alocada
		  free(fn);
		  return 0;
}
