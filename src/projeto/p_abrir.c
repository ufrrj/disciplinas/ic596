#include <stdio.h>
#include <stdlib.h>

#include "p_funcoes.h"

FILE *
abrir_arquivo(char *fn, FILE *fp)
{ 
	if ( (fp = fopen(fn, "r+")) == (FILE *)(NULL) ) 
	{ 
		printf("Arquivo padrão não existe!\n"); 
		printf("Arquivo padrão será criado!\n"); 
		if ( (fp = fopen(fn, "wt")) == (FILE *)(NULL) ) 
			perror("Erro na criação do arquivo padrão"); 
	} 
	fseek(fp, 0L, SEEK_END);
	return fp;
}
