#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <errno.h>

#include "p_funcoes.h"

#define	FN		"./agenda.txt"

int main(void);

int
main(void)
{
	pessoa p;
	unsigned int op, c;
	char fn[256];
	FILE *fp;
	unsigned int registros, lastid;
	ssize_t l;

	printf("Programa agenda\n");
	printf("---------------\n");
	
	fp = (FILE *)(NULL);
	for( c=0; c <256; c++)
			  fn[c] = '\0';
	memcpy(fn, FN, strlen(FN));

	if (( fp = abrir_arquivo(fn, fp)) == (FILE *)(NULL))
			  perror("Falha ao processar arquivo");
	registros  = qregistros(&lastid, fp);
	fclose(fp);

	printf("Arquivo padrão: %s\n", fn);
	printf("Total de registros: %u\n", registros);
	printf("Last ID = %u\n", lastid);


	while ( 1 )
	{
			  op = menu();
			  printf("Voce escolheu a opcao %c ( %u ) \n", op, op );
			  switch ( op )
			  { 
						 case 1: 
									abrir_arquivo(fn, fp);
									inserir(&p); 
									p.id = ++lastid; 
									if ( gravar(p, fp) )
									{ 
											  printf("\n**** Erro ao gravar registro"); 
											  lastid--; 
									}
									else
											  printf("\nRegistro gravado com sucesso\n");
									fclose(fp);
									break;
						 case 2:
									abrir_arquivo(fn, fp);
									rewind(fp);
									if ( (l = buscar(&p, fp)) <= 0)
											  printf("Não retornou nenhum registro\n");
									else
											  printf("Encontrado %zu registro(s)\n", l);
									fclose(fp);
									break;
						 case 3:
									printf("Opacao 3\n");
									break;
						 case 4:
									abrir_arquivo(fn, fp);
									rewind(fp);
									while ( (l = ler(&p, fp)) > 0 )
									{
											  printf("ID ......: %d\n", p.id   );
											  printf("NOME ....: %s\n", p.nome );
											  printf("CPF .....: %s\n", p.cpf  );
											  printf("TEL .....: %s\n", p.tel  );
											  printf("IDADE ...: %d\n", p.idade);
									}
														
									if ( l > 0 )
											  printf("Leitura ok\n");
									fclose(fp);
									break;
						 case 9:
									printf("Finalizando programa\n");
									return 0;
									break;
			  }
	}

	return 0;
}

