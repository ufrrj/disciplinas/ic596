#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <errno.h>

#include "p_funcoes.h"


unsigned int
qregistros(unsigned int *lastid, FILE *fp)	
{
		  unsigned int r;
		  pessoa p;
		  ssize_t l;

		  rewind(fp);
		  *lastid=0;
		  r = 0;
		  while ( (l = ler( &p, fp) ) > 0 )
		  {
					 *lastid = p.id;
					 r++;
		  }

		  rewind(fp);
		  return r;
}

ssize_t
lerlinha(char **buffer, FILE *fp)
{ 
	size_t	len; 
	ssize_t 	slen; 
	
	len=0; 
	*buffer = NULL;
	slen = getdelim(buffer, &len, '@', fp); 
	
	if ( slen > 0 && !feof(fp) ) 
	{
		*(*buffer+slen-1) = '\0'; 
	}
	else
	{
		slen = -1;
	}
	return slen;
}

ssize_t
ler( pessoa *p, FILE *fp)
{
	char *buffer, *aux;
	ssize_t letras;

	buffer = NULL;
		letras = lerlinha(&buffer, fp);
		aux = buffer;
		if ( letras > 0 )
		{ 
				  strsep(&buffer, ":"); 
				  p->id = atoi(strsep(&buffer, ":")); 
				  memcpy(p->nome, strsep(&buffer, ":"), 100); 
				  memcpy(p->cpf , strsep(&buffer, ":"),  12); 
				  memcpy(p->tel , strsep(&buffer, ":"),  15); 
				  p->idade = atoi(strsep(&buffer, ":"));
		} 
		free(aux);
	return letras;
}

ssize_t
buscar( pessoa *p, FILE *fp)
{
		  ssize_t l, counter;;
		  size_t len;
		  char *buffer;

		  cleanup_pessoa(p);
		  buffer = NULL;
		  len = 0;
		  printf("Digite o nome a busca: ");
		  l = getline(&buffer, &len, stdin);
		  if ( l <= 0  || feof(fp) )
					 return l;
		  buffer[l - 1] = '\0';
		  counter = 0;
		  do
		  {
					 l = ler( p, fp);
					 if ( l > 0  && strstr (p->nome, buffer) != NULL)
					 {
								printf("ID    : %d :\n", p->id);
								printf("NOME  : %s :\n", p->nome);
								printf("CPF   : %s :\n", p->cpf);
								printf("TEL   : %s :\n", p->tel);
								printf("IDADE : %d :\n", p->idade);
								counter ++;
					 }
		  }
		  while ( l > 0 );
		  return counter;
}
