#include <stdio.h>
#include <stdlib.h>

unsigned int menu(void);

unsigned int
menu(void)
{
	unsigned int op;

	do 
	{
		printf("\n\
\t+---------------------------------+\n\
\t|                                 |\n\
\t|                                 |\n\
\t|\t(1) - Inserir dados       |\n\
\t|\t(2) - Buscar dados        |\n\
\t|\t(3) - Gravar dados        |\n\
\t|\t(4) - Ler dados           |\n\
\t|\t(9) - Fim                 |\n\
\t|                                 |\n\
\t+---------------------------------+\n\
\t\t\t\tOpcao: ");
		fflush(stdout);
		scanf("%u", &op);
		getchar();
		printf("\n");
	}
	while(( op < 1 || op > 4 ) && op != 9);
	return op;
}

