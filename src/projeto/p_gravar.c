#include <stdio.h>
#include <stdlib.h>


#include "p_funcoes.h"

unsigned int 
gravar(pessoa p, FILE *fp)
{
		  int e;

		  e = fprintf(fp, "%c", FIELDSEP);
		  printf("%d ", e); fflush(stdout);
		  e = fprintf(fp, "%d%c", p.id, FIELDSEP);
		  printf("%d ", e); fflush(stdout);
		  e = fprintf(fp, "%s%c", p.nome, FIELDSEP);
		  printf("%d ", e); fflush(stdout);
		  e = fprintf(fp, "%s%c", p.cpf, FIELDSEP);
		  printf("%d ", e); fflush(stdout);
		  e = fprintf(fp, "%s%c", p.tel, FIELDSEP);
		  printf("%d ", e); fflush(stdout);
		  e = fprintf(fp, "%u%c", p.idade, FIELDSEP);
		  printf("%d ", e); fflush(stdout);
		  e = fprintf(fp, "%s", ENDROW);
		  printf("%d ", e); fflush(stdout);

		  e = ( e < 0 )?1:0;

		  return e;
}
