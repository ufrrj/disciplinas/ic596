#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "p_funcoes.h"

unsigned int inserir(pessoa *p);

unsigned int inserir(pessoa *p)
{
	char *str;
	size_t len;
	ssize_t glen;

	printf("Digite o nome: ");
	len = 0;
	str = NULL;
	glen = getline(&str, &len, stdin);
	str[glen-1] = '\0';
	memcpy(p->nome, str, 100);
	free(str);
	printf("Digite o CPF: ");
	len = 0;
	str = NULL;
	glen = getline( &str, &len, stdin);
	str[glen-1] = '\0';
	memcpy(p->cpf, str, 12);
	free(str);
	len = 0;
	str = NULL;
	printf("Digite o telefone: ");
	glen = getline( &str, &len, stdin);
	str[glen-1] = '\0';
	memcpy( p->tel, str, 15);
	free(str);
	printf("digite a idade: ");
	scanf("%u", &p->idade);
	return 0;
}
