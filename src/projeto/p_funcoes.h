#ifndef __P_FUNCOES_H__
#define __P_FUNCOES_H__

#define	STARTROW		"$"
#define  FIELDSEP		':'
#define  ENDROW		"@"


typedef struct P {
	unsigned int id;
	char nome[100];
	char cpf[12];
	char tel[15];
	unsigned int idade;
} pessoa;

// Funcao para mostrar o menu
unsigned int menu(void);
unsigned int inserir(pessoa *p);
unsigned int gravar(pessoa p, FILE *fp);
unsigned int qregistros(unsigned int *lastid, FILE *fp);
ssize_t ler(pessoa *p, FILE *fp);
ssize_t buscar(pessoa *p, FILE *fp);

ssize_t lerlinha(char **buffer, FILE *fp);
FILE * abrir_arquivo(char *fn, FILE *fp);

void cleanup_pessoa(pessoa *p);

#endif
