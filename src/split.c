#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <string.h>

/*
 * Programa para dividir a string em um separador qualquer.
 * O programa recebe uma string em line, divide a string utilizando
 * o separador que está em sep, armazenando as strings em um vetor
 * de ponteiros split
 *
 * Funções utilizadas:
 *
 * size_t getline(char restrict **buffer, ssize_t restrict *buffcap, 
 * 					FILE retrict *file)
 * 		A função getline le de um arquivo, no caso stdin, e retorna a
 * 	string dentro do buffer. Caso o buffer seja NULL, a função aloca 
 * 	adequadamente e retorna o buffer uma string com o caracter '\n' no
 * 	final e o valor que retorna é a quantidade de caracteres digitados
 * 	ou lidos do arquivo, incluindo o '\n'
 * 	Caso o buffer serja alocado fora da função, buffcap deve conter o 
 * 	tamanho do buffer e a função getline, irá realocar o buffer se 
 * 	for necessário para caber o que está sendo lido. 
 * int err(int eval, const char *fmt, ...,)
 * 		Esta função mostra uma mensagem de erro e encerra o programa, 
 * 	retornando para o sistema um código de erro
 * char *strcpy(char restrict *dst, const char restrict *src)
 * 		Copia a string src para a string dst, retornando um ponteiro para
 * 	a string dst
 *
 */
int main(int argc, char *argv[]);

int
main(int argc, char *argv[])
{
   char *line, *tofree, *copy, **split;
   char *p;
   char sep;
   size_t len, slen, counter, i, last, maior, value;

   sep = ' '; // Separador
	slen = 0;
	line = NULL;
   printf("Digite uma frase qualquer: ");
   if ( (len = getline(&line, &slen, stdin)) == -1 )
			  err(1, "Error on getline function");
	/*
	 * muda o '\n' para '\0'
	 */
	line[len - 1] = '\0';
	copy = line;
	counter = last = maior = 0;
	/*
	 * procura pela maior para palavra, para poder alocar corretamente
	 * os espaços para armazenar as partes.
	 */
	for ( i = 0; i <len; i++ )
	{
			  /*
				* procura pelo separadou ou pelo fim da string
				*/
			  if ( copy[i] == sep || copy[i] == '\0' )
			  {
						 value = i - last;
						 if ( value > maior )
									maior = value;
						 last = i;
						 counter ++;
			  }
	}
	tofree = (char  *)(calloc(sizeof(char  ), len));
	if ( tofree == (char *)(NULL) )
			  err(2,"Erro na alocacao de tofree");
	/*
	 * Aloca o vetor de ponteiros
	 */
	split  = (char **)(calloc(sizeof(char *), counter));
	if ( split == (char **)(NULL) )
			  err(2,"Erro na alocacao de split");
	/*
	 * Aloca espaço para cada string no vetor
	 */
	for ( i = 0; i < counter; i ++ )
	{
			  split[i] = (char *)(calloc(sizeof(char), maior));
			  if ( split[i] == (char *)(NULL) )
						 err(3,"Erro na alocacao de spĺit[%lu]\n", i);
	}

	strcpy(tofree,line);
	p = tofree;
   for ( value = 0, i = 0; i <= len; i ++ )
   {
		/*
		 * Procura pelo separador ou pelo final da string
		 */
      if ( line[i] == sep || line[i] == '\0' )
      {
			/*
			 * coloca o '\0' na cópia da string para dividir a string
			 */
         tofree[i] = '\0';
			strcpy(split[value], p);
         printf("%s\n", split[value]); 
			i++;
			value ++;
			/* Move o ponteiro auxiliar da string para o próximo caracter
			 * após o separador sep
			 */
			p = tofree + i;
      }
   }
	/*
	 * libera os ponteiros alocados
	 * O duplo ponteiro deve ser desalocado na ordem inversa da alocação,
	 * ou seja, primeiro os ponteiros para as string e depois o vetor
	 * de ponteiros
	 */
   free(tofree);
   free(line);
	for( value=0; value < counter; value ++)
			  free(split[value]);
	free(*split);
   return (0);
}
