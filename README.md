# IC596 Class

## Programming language II

This repository is to support students in the IC596 class at Federal Rural University of Rio de Janeiro, in Seropédica, Rio de Janeiro, Brazil.

All students can get these files to improve their programming skills.

The website with more details can be found [here](https://www.rizzo.eng.br/ufrrj/ic596/index.php).


To compile any file, use the [clang](https://www.llvm.org) or gcc compiler.
On terminal, type this:
```
    clang -o filename filename.c

            or

    gcc -o filename filename.c
```
